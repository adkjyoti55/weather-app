import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

final weatherApiProvider = Provider<WeatherApi>((ref) {
  return WeatherApi();
});

final weatherDataNotifierProvider =
    StateNotifierProvider<WeatherDataNotifier, WeatherData>((ref) {
  return WeatherDataNotifier(ref.watch(weatherApiProvider));
});

final locationNameProvider = FutureProvider<String>((ref) async {
  final storage = FlutterSecureStorage();
  return await storage.read(key: 'location') ?? '';
});

final locationNameNotifierProvider =
    StateNotifierProvider<LocationNameNotifier, String>((ref) {
  final locationName = ref.watch(locationNameProvider);
  return LocationNameNotifier(locationName.value ?? "");
});

class LocationNameNotifier extends StateNotifier<String> {
  LocationNameNotifier(String locationName) : super(locationName);

  void updateLocationName(String newLocation) async {
    final storage = FlutterSecureStorage();
    await storage.write(key: 'location', value: newLocation);
    state = newLocation;
  }
}

class WeatherDataNotifier extends StateNotifier<WeatherData> {
  final WeatherApi weatherApi;

  WeatherDataNotifier(this.weatherApi) : super(WeatherData());

  Future<void> fetchWeatherData(String location) async {
    try {
      final weatherData = await weatherApi.fetchWeatherData(location);
      state = WeatherData(
        temperature: weatherData['current']['temp_c'].toString(),
        iconUrl: weatherData['current']['condition']['icon'],
      );
    } catch (e) {
      print('Failed to fetch weather data: $e');
    }
  }
}

class WeatherApi {
  final String apiKey = 'YOUR_API_KEY';
  final Dio dio = Dio();

  Future<Map<String, dynamic>> fetchWeatherData(String location) async {
    try {
      final response = await dio.get(
        'http://api.weatherapi.com/v1/current.json',
        queryParameters: {
          'key': apiKey,
          'q': location,
        },
      );
      if (response.statusCode == 200) {
        return response.data;
      } else {
        throw Exception('Failed to fetch weather data');
      }
    } catch (e) {
      throw Exception('Failed to fetch weather data');
    }
  }
}

class WeatherData {
  final String temperature;
  final String iconUrl;

  WeatherData({this.temperature = '', this.iconUrl = ''});
}

class WeatherRequest {
  final String inputLocation;

  WeatherRequest({required this.inputLocation});
}
