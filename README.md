# Ncash Weather App

The Ncash Weather App is a simple weather application built using Flutter. It allows users to get the current weather information for a specific location.

## Getting Started

To get started with the Ncash Weather App, follow the steps below:

### Prerequisites

Make sure you have Flutter and Dart installed on your local machine. You can download and install Flutter from the official Flutter website: [https://flutter.dev](https://flutter.dev).

### Installation

1. Clone the repository to your local machine using the following command:
```
  git clone https://gitlab.com/your-repo-link.git
  ```

2. Navigate to the project directory:

```
cd ncash_weather_app
```

3. Install the dependencies by running the following command:

```
flutter pub get
```

### Configuration

1. Obtain an API key from WeatherAPI by signing up on their website: [https://www.weatherapi.com](https://www.weatherapi.com).

2. Replace `locationApiKey` with your actual API key in the `constants.dart` file:
```
const String locationApiKey = 'your_api_key';
const String BASE_URL = "http://api.weatherapi.com/v1/current.json";
const String LOCATION_BY_NAME =
    "http://api.weatherapi.com/v1/current.json?key=$locationApiKey&q=%s";
```

- (Optional) If you want to use a different weather API, you can modify the `constants.dart` file to make the appropriate API calls.

### Usage
Run the app on an emulator or physical device using the following command:
flutter run

1. The app will launch with the Get Started screen. You can click the "Skip" button to directly go to the homepage or wait for the app to automatically redirect after 4 seconds.

2. On the Homepage screen, enter a location name in the textbox and click the "Save" button to get the weather information for that location. If the location name is left blank, the app will use the current latitude and longitude to fetch the weather data.

3. The app will display the temperature and weather icon for the selected location. The temperature will be shown in Celsius.

4. To update the location or get weather information for a different location, enter a new location name in the textbox and click the "Update" button.

5. The app will remember the location name for subsequent launches using Flutter Secure Storage.

### Testing
To run the tests for the Ncash Weather App, use the following command on the terminal:

```
flutter test
```

The tests cover critical functionalities of the app, including API calls, data storage, and UI interactions.

### Contributing
Contributions to the Ncash Weather App are welcome! If you find any issues or have suggestions for improvement, please open an issue or submit a pull request on GitLab.

### License
This project is licensed under the [MIT License](https://opensource.org/license/mit/).
