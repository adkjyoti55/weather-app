import 'package:flutter/material.dart';

/// tablet view breakpoint i.e. 768
const double kTabletBreakpoint = 768.0;

/// 18
const double kDefaultFontSize = 18;

///20
const double kDefaultPadding = 20;

///20
const double kDefaultMargin = 20;

///20
const double kDefaultRadius = 20;
const double kNavBarHeight = 75;
//colors
const Color kScaffoldColor = Color(0xFFF5F6FA);
const kPrimaryColor = Color(0xFFFBDA6E);
const kSecondaryColor = Color(0xFFFBDA6E);
const kWhiteColor = Colors.white;
const kBlackColor = Colors.black;
const kGreyColor = Colors.grey;
const kDarkGrey = Color(0xFF707070);
const kLightGrey = Color(0xFFF3F2F2);
const kLightGreyBorder = Color(0xFFD9D9D9);
const kHintTextColor = Color(0xFF858585);
const kTextColor = Color(0xFF3D3D3D);
const kGreenColor = Color(0xFF289720);
const kYellowColor = Color(0xFFF5EF3B);
const kLightGreen = Color(0xFFDEE854);
const kLightRed = Color(0xFFFF4F4F);
const kRedColor = Color(0xFFD31D35);
const kLimeColor = Color(0xFFCCD227);
const Color grey = Color(0xFF808080);
const Color lightGrey = Color(0xFFDED9D9);
const Color nGrey = Color(0xFFDADADA);
const Color darkGrey = Color(0xFF787777);
const Color textGrey = Color(0xFF707070);
const Color yellow = Color(0xFFFFDA4B);
const Color orange = Color(0xFFFFC25A);
const Color shadow = Color(0xFFD7D7F3);
const Color pink = Color(0xFFFA9190);
const Color gYellow = Color(0xFFFFDF3E);
const Color bottom = Color(0xFFFFC25A);

const kErrorColor = Colors.red;

const Color kButtonColor = Color(0xFFF2A32B);
//sign in background color
const Color kSignBackgroundColor = Color(0xFFF7F7F7);

const Color kFollowerTextColor = Color(0xFF696969);
const Color kDarkOrange = Color(0xFFE89B26);

const Color kLightestGrey = Color(0xFFEEEEEE);

const Color kCircleButtonColor = Color(0xFFD0D0D0);

const Color kDrawerCircleColor = Color(0xFFDAD7D7);
// const Color kYellowColor = Color(0xFFF9BE21);

const String locationApiKey = '729323b7eac24afa95e93042232506';
const String BASE_URL = "http://api.weatherapi.com/v1/current.json";
const String LOCATION_BY_NAME =
    "http://api.weatherapi.com/v1/current.json?key=$locationApiKey&q=%s";
//const String LOCATION_BY_COORDINATE = "http://api.weatherapi.com/v1/current.json?key=$locationApiKey&q=$latitude,$longitude";
const String LOCATION_BY_COORDINATE =
    "http://api.weatherapi.com/v1/current.json?key=$locationApiKey&q=%s,%s";
