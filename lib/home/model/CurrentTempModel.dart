import 'package:json_annotation/json_annotation.dart';
import 'package:weather_app/home/model/ConditionModel.dart';

part 'CurrentTempModel.g.dart';

@JsonSerializable()
class CurrentTempModel {
  final int? lastUpdatedEpoch;
  final String? lastUpdated;
  final double? temp_c;
  final double? temp_f;
  final int? isDay;
  final ConditionModel? condition;
  final double? windMph;
  final double? windKph;
  final int? windDegree;
  final String? windDir;
  final int? pressureMb;
  final double? pressureIn;
  final double? precipMm;
  final double? precipIn;
  final int? humidity;
  final int? cloud;
  final double? feelslikeC;
  final double? feelslikeF;
  final int? visKm;
  final int? visMiles;
  final double? uv;
  final double? gustMph;
  final double? gustKph;

  CurrentTempModel(
      {this.lastUpdatedEpoch,
      this.lastUpdated,
      this.temp_c,
      this.temp_f,
      this.isDay,
      this.condition,
      this.windMph,
      this.windKph,
      this.windDegree,
      this.windDir,
      this.pressureMb,
      this.pressureIn,
      this.precipMm,
      this.precipIn,
      this.humidity,
      this.cloud,
      this.feelslikeC,
      this.feelslikeF,
      this.visKm,
      this.visMiles,
      this.uv,
      this.gustMph,
      this.gustKph});

  factory CurrentTempModel.fromJson(Map<String, dynamic> json) =>
      _$CurrentTempModelFromJson(json);

  Map<String, dynamic> toJson() => _$CurrentTempModelToJson(this);
}
