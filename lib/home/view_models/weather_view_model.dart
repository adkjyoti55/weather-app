import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:weather_app/home/model/WeatherResponseModel.dart';
import 'package:weather_app/home/repository/home_repo/WeatherByCoordinatesRepo.dart';
import 'package:weather_app/network_utils/DioManager.dart';
import 'package:weather_app/network_utils/Extensions.dart';
import 'package:weather_app/network_utils/NetworkState.dart';

final currentLocationProviderRef = FutureProvider<Position>((ref) async {
  final position = await Geolocator.getCurrentPosition(
    desiredAccuracy: LocationAccuracy.high,
  );
  return position;
});

final weatherChangeNotifierProvider =
    ChangeNotifierProvider((ref) => WeatherViewModelProvider());

final storageProvider = Provider<FlutterSecureStorage>((ref) {
  return FlutterSecureStorage();
});

class WeatherViewModelProvider with ChangeNotifier {
  WeatherResponseModel? get body => _joinNetworkState.response?.body;

  String get error =>
      _joinNetworkState.response?.exception?.message ?? "Something went wrong";

  NetworkState<WeatherResponseModel> _joinNetworkState = NetworkState.ideal();

  NetworkState<WeatherResponseModel> get joinNetworkState => _joinNetworkState;

  WeatherResponseModel? _weatherResponseModel;

  WeatherResponseModel? get weatherResponseModel => _weatherResponseModel;
  String _inputLocation = "";
  String get inputLocation => _inputLocation;

  String _inputLocationTemp = "";
  String get inputLocationTemp => _inputLocationTemp;

  void fetchWeatherDataByCoordinates(double? inputLat, double? inputLong) {
    final WeatherByCoordinatesRepo weatherApiRepository =
        WeatherByCoordinatesRepo(inputLat: inputLat, inputLong: inputLong);
    weatherApiRepository.fetch(
      baseUrlType: BaseUrlType.LOCATIONBYCOORDINATE,
      apiCallback: (networkState) {
        onApiCallback<WeatherResponseModel>(
          networkState: networkState,
          onLoadedState: (loadedState) {
            onFutureNotifyListeners(() async {
              _weatherResponseModel = loadedState.response?.body;
              showMyToast(
                  _weatherResponseModel?.current.temp_c.toString() ?? "Error");
            });
          },
          onErrorState: (errorState) {
            onFutureNotifyListeners(() {
              _weatherResponseModel = errorState.response?.body;
              showMyToast("${errorState.response?.body}");
            });
          },
          onLoadingState: (loadingState) {},
        );
      },
    );
  }
}
