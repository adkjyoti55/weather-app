import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:weather_app/network_utils/NetworkState.dart';
import 'package:weather_app/ui/utils/text_style.dart';

dynamic importExtensions;

typedef LoadingStateCallback<T> = Function(NetworkState<T> loadingState);
typedef ErrorStateCallback<T> = Function(NetworkState<T> errorState);
typedef LoadedStateCallback<T> = Function(NetworkState<T> loadedState);
typedef ApiCallback<T> = void Function(NetworkState<T> networkState);

typedef IndexItem<T> = Function(int index, T item);

void addMyPostFrameCallback(FrameCallback callback) {
  WidgetsBinding.instance.addPostFrameCallback(callback);
}

void showMySnackBar(BuildContext context, String message,
    [bool removeCurrent = true,
    Duration duration = const Duration(milliseconds: 900)]) {
  addMyPostFrameCallback((_) {
    if (removeCurrent) ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        duration: duration,
        content: Text(
          message,
          style: TextStyleUtils.body2TextStyle,
        )));
  });
}

void showMyToast(String message,
    {Toast toastLength: Toast.LENGTH_SHORT, Color? backgroundColor}) {
  Fluttertoast.cancel();
  Fluttertoast.showToast(
      msg: message,
      toastLength: toastLength,
      fontSize: 15,
      backgroundColor: backgroundColor,
      timeInSecForIosWeb: 1);
}

extension ChangeNotifierExt on ChangeNotifier {
  void onApiCallback<T>(
      {required NetworkState<T> networkState,
      required LoadingStateCallback<T>? onLoadingState,
      required ErrorStateCallback<T>? onErrorState,
      required LoadedStateCallback<T>? onLoadedState}) {
    if (networkState.state == DataFetchState.ERROR_ENCOUNTERED) {
      onErrorState?.call(networkState);
    } else if (networkState.state == DataFetchState.IS_LOADED) {
      onLoadedState?.call(networkState);
    } else if (networkState.state == DataFetchState.IS_LOADING) {
      onLoadingState?.call(networkState);
    }
  }

  void onFutureNotifyListeners(void Function() onNotify) {
    onNotify();
    futureNotifyListeners();
  }

  void futureNotifyListeners() {
    Future.delayed(Duration.zero, () => notifyListeners());
  }
}

extension MapExt on Map {
  String jsonEncode() {
    return json.encode(this);
  }
}

extension StringExt on String {
  dynamic jsonDecode() {
    return json.decode(this);
  }
}
