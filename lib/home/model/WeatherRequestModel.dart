import 'package:json_annotation/json_annotation.dart';

part 'WeatherRequestModel.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class WeatherRequestModel {
  final String? location;

  WeatherRequestModel({this.location});

  factory WeatherRequestModel.fromJson(Map<String, dynamic> json) =>
      _$WeatherRequestModelFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherRequestModelToJson(this);
}
