import 'package:flutter/material.dart';
import 'package:weather_app/ui/constant/constants.dart';
import 'package:weather_app/ui/utils/app_size_utils.dart';
import 'package:weather_app/ui/utils/customheight.dart';
import 'package:weather_app/ui/utils/text_style.dart';
import 'package:weather_app/ui/widgets/custom_normal_text_widget.dart';

/// custom raised/elevated button widget
class CustomElevatedButton extends StatelessWidget {
  const CustomElevatedButton(
      {Key? key,
      required this.label,
      required this.onPressed,
      this.backgroundColor = kPrimaryColor,
      this.disabled = false,
      this.fontSize = kDefaultFontSize,
      this.width,
      this.textColor,
      this.image,
      this.showImage = false,
      this.isUpper = false,
      this.h,
      this.secondText,
      this.imageHeight = 20.0,
      this.borderRadius = true,
      this.buttonBorderColor,
      this.radius = 20.0})
      : super(key: key);

  final bool isUpper;
  final double? width;
  final VoidCallback onPressed;
  final String label;
  final Color? backgroundColor;
  final bool disabled;
  final double fontSize;
  final Color? textColor;
  final String? image;
  final bool showImage;
  final double imageHeight;
  final double? h;
  final bool borderRadius;
  final String? secondText;
  final double radius;
  final Color? buttonBorderColor;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 8),
      constraints: BoxConstraints(minHeight: h ?? height(65)),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(29)),
      width: width ?? double.infinity,
      child: ElevatedButton(
        onPressed: disabled ? () {} : onPressed,
        style: !disabled
            ? ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(backgroundColor ?? kButtonColor),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: borderRadius
                            ? BorderRadius.circular(radius)
                            : BorderRadius.zero,
                        side: BorderSide(
                            color: buttonBorderColor ?? backgroundColor!))))
            : ElevatedButton.styleFrom(
                primary: kGreyColor, // background
                onPrimary: kWhiteColor, // text color
              ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            showImage
                ? Image.asset(
                    image!,
                    height: imageHeight,
                  )
                : const SizedBox(),
            showImage ? WidthWidget(8.0) : const SizedBox(),
            Text(
              label.trim(),
              style: TextStyleUtils.body1TextStyle
                  .copyWith(fontSize: 14, fontWeight: FontWeight.w500),
            ),
            if (secondText != null)
              TextWidget(
                " " + secondText!,
                fontSize: fontSize,
                color: textColor ?? kWhiteColor,
                isUpper: isUpper,
              ),
          ],
        ),
      ),
    );
  }
}
