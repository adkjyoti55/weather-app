// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'WeatherResponseModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherResponseModel _$WeatherResponseModelFromJson(Map<String, dynamic> json) {
  return WeatherResponseModel(
    location: LocationModel.fromJson(json['location'] as Map<String, dynamic>),
    current: CurrentTempModel.fromJson(json['current'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$WeatherResponseModelToJson(
        WeatherResponseModel instance) =>
    <String, dynamic>{
      'location': instance.location,
      'current': instance.current,
    };
