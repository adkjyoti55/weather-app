import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:weather_app/home/view/home_view.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final FlutterSecureStorage _secureStorage = FlutterSecureStorage();
  @override
  void initState() {
    super.initState();
    _checkSkipPref();
    // Future.delayed(
    //     const Duration(seconds: 4),
    //     () => Navigator.pushReplacement(
    //           context,
    //           MaterialPageRoute(builder: (context) => const HomePage()),
    //         ));
  }

  Future<void> _checkSkipPref() async {
    bool skipped = await _secureStorage.read(key: 'skipped') == 'true';
    if (skipped) {
      _navigateToHome();
    } else {
      Timer(const Duration(seconds: 4), () {
        _navigateToHome();
      });
    }
  }

  void _navigateToHome() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => HomePage()),
    );
  }

  void _handleSkip() async {
    await _secureStorage.write(key: 'skipped', value: 'true');
    _navigateToHome();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ncash Weather App"),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Image.asset(
          'assets/images/splash_bg.jpg',
          fit: BoxFit.cover,
          repeat: ImageRepeat.noRepeat,
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => _handleSkip,
        label: const Text(
          'Skip',
          style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
        ),
        icon: const Icon(Icons.arrow_right_alt_outlined),
        hoverColor: Colors.lightBlueAccent,
        tooltip: "Go to Home Screen",
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
