import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:weather_app/network_utils/Extensions.dart';
import 'package:weather_app/ui/constant/constants.dart';

class DioManager {
  static DioManager? _instance;

  static DioManager get instance => _instance ??= DioManager._init();
  final DioCacheManager _dioCacheManager = DioCacheManager(CacheConfig());
  final Dio _defaultDio = Dio(BaseOptions(
    baseUrl: BASE_URL,
    connectTimeout: 10000,
    receiveTimeout: 10000,
    sendTimeout: 10000,
  ));

  final Dio _locationByNameDio = Dio(BaseOptions(
    baseUrl: LOCATION_BY_NAME,
    connectTimeout: 10000,
    receiveTimeout: 10000,
    sendTimeout: 10000,
  ));

  final Dio _locationByCoordinate = Dio(BaseOptions(
    baseUrl: LOCATION_BY_COORDINATE,
    connectTimeout: 10000,
    receiveTimeout: 10000,
    sendTimeout: 10000,
  ));

  DioManager._init() {
    initDio(_defaultDio, false);
    initDio(_locationByNameDio, false);
    initDio(_locationByCoordinate, false);
  }

  void initDio(Dio dio, bool shouldRetry) {
    dio.interceptors.add(_dioCacheManager.interceptor);
    dio.interceptors.add(ErrorInterceptor());
  }

  Dio getDio(BaseUrlType baseUrlType) {
    switch (baseUrlType) {
      case BaseUrlType.DEFAULT:
        return _defaultDio;
      case BaseUrlType.LOCATIONBYNAME:
        return _locationByNameDio;
      case BaseUrlType.LOCATIONBYCOORDINATE:
        return _locationByCoordinate;
    }
  }

  Future<Response<T>> get<T>(
      {required String path,
      shouldCache = false,
      BaseUrlType baseUrlType = BaseUrlType.DEFAULT,
      Map<String, dynamic>? headers}) async {
    if (shouldCache) {
      Options _cacheOptions = buildCacheOptions(Duration(days: 3),
          maxStale: Duration(days: 5), forceRefresh: true);
      if (headers != null) {
        _cacheOptions.headers?.addAll(headers);
      }
      return await getDio(baseUrlType).get(path, options: _cacheOptions);
    } else {
      final options = Options(headers: {});
      if (headers != null) {
        options.headers?.addAll(headers);
      }
      return await getDio(baseUrlType).get(path, options: options);
    }
  }
}

class ErrorInterceptor extends Interceptor {
  var connectivityResult;

  @override
  Future<void> onError(DioError err, ErrorInterceptorHandler handler) async {
    // debugPrintStack(label: err.response.toString(), maxFrames: 50);
    final ConnectivityResult result = await Connectivity().checkConnectivity();

    if (result == ConnectivityResult.none) {
      return handler.next(NoInternetConnectionException(err.requestOptions));
    }

    if (err.response?.data != null) {
      return handler.next(ServerException(err.requestOptions,
          err.response!.data, err.response?.statusCode ?? 0));
    }

    switch (err.type) {
      case DioErrorType.connectTimeout:
      case DioErrorType.sendTimeout:
      case DioErrorType.receiveTimeout:
        return handler.next(DeadlineExceededException(err.requestOptions));
      case DioErrorType.response:
        switch (err.response?.statusCode) {
          case 400:
            return handler.next(BadRequestException(err.requestOptions));
          case 401:
            return handler
                .next(throw UnauthorizedException(err.requestOptions));
          case 404:
            return handler.next(NotFoundException(err.requestOptions));
          case 409:
            return handler.next(ConflictException(err.requestOptions));
          case 500:
            return handler
                .next(InternalServerErrorException(err.requestOptions));
        }
        break;
      case DioErrorType.cancel:
        break;
      case DioErrorType.other:
        return handler.next(InternalServerErrorException(err.requestOptions));
    }

    return handler.next(err);
  }
}

class BadRequestException extends DioError {
  BadRequestException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'Invalid request';
  }
}

class InternalServerErrorException extends DioError {
  InternalServerErrorException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'Unknown error occurred, please try again later.';
  }
}

class ConflictException extends DioError {
  ConflictException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'Conflict occurred';
  }
}

class UnauthorizedException extends DioError {
  UnauthorizedException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'Access denied';
  }
}

class NotFoundException extends DioError {
  NotFoundException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'The requested information could not be found';
  }
}

class NoInternetConnectionException extends DioError {
  NoInternetConnectionException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return ''
        'No internet connection detected, please try again.';
  }
}

class DeadlineExceededException extends DioError {
  DeadlineExceededException(RequestOptions r) : super(requestOptions: r);

  @override
  String toString() {
    return 'The connection has timed out, please try again.';
  }
}

class ServerException extends DioError {
  final Map<String, dynamic> errorMessage;
  final int statusCode;

  ServerException(RequestOptions r, this.errorMessage, this.statusCode)
      : super(requestOptions: r);

  @override
  String toString() {
    // final Map<String, dynamic> errorResponse = errorMessage.jsonEncode();
    if (errorMessage.containsKey("message")) {
      return "Status Code: $statusCode, ${errorMessage["message"]}";
    }
    return "Status Code: $statusCode, ${errorMessage.jsonEncode()}";
  }
}

enum BaseUrlType { DEFAULT, LOCATIONBYNAME, LOCATIONBYCOORDINATE }
