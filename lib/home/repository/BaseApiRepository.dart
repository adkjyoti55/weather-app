import 'package:dio/dio.dart';
import 'package:weather_app/network_utils/DioManager.dart';
import 'package:weather_app/network_utils/Extensions.dart';
import 'package:weather_app/network_utils/NetworkState.dart';

abstract class BaseApiRepository<T> {
  String path(Map<String, dynamic>? params);

  T parseJson(Response response);

  void fetch(
      {Map<String, dynamic>? params,
      required ApiCallback<T> apiCallback,
      BaseUrlType baseUrlType = BaseUrlType.DEFAULT}) async {
    apiCallback(NetworkState.loading());
    try {
      final response = await DioManager.instance
          .get(path: path(params), baseUrlType: baseUrlType);
      if (response.statusCode == 200 || response.statusCode == 201) {
        apiCallback(NetworkState.loaded(
            ApiResponse.response(body: parseJson(response))));
      } else {
        apiCallback(NetworkState.error(ApiResponse.error(
            exception: ApiException(response.data.toString()))));
      }
    } on DioError catch (error) {
      apiCallback(NetworkState.error(
          ApiResponse.error(exception: ApiException(error.toString()))));
    }
  }
}
