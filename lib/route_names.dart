class RouteNames {
  RouteNames._();

  static const String gettingStartedPageRoute = "/";
  static const String homePageRoute = "/login";
}
