import 'package:flutter/material.dart';
import 'package:weather_app/ui/constant/constants.dart';

class TextWidget extends StatelessWidget {
  final double? fontSize;
  final bool isBold;
  final bool hasUnderline;
  final String? text;
  final bool isCentered;
  final Color? color;
  final int? maxLine;
  final double? letterSpacing;
  final double? lineHeight;
  final FontWeight? fontWeight;
  final bool isUpper;
  final double? minFontSize;
  const TextWidget(
    this.text, {
    Key? key,
    this.isUpper = false,
    this.fontSize,
    this.minFontSize,
    this.fontWeight,
    this.lineHeight,
    this.isBold = false,
    this.hasUnderline = false,
    this.isCentered = false,
    this.color,
    this.maxLine,
    this.letterSpacing,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      isUpper ? text ?? "".toUpperCase() : text ?? "",
      style: Theme.of(context).textTheme.bodyText1!.copyWith(
          fontFamily: 'Gilroy',
          fontSize: fontSize ?? kDefaultFontSize,
          fontWeight:
              isBold ? FontWeight.bold : fontWeight ?? FontWeight.normal,
          decoration: hasUnderline ? TextDecoration.underline : null,
          color: color ?? kTextColor,
          height: lineHeight ?? 1.2,
          letterSpacing: letterSpacing),
      textAlign: isCentered ? TextAlign.center : null,
      maxLines: maxLine,
      overflow: maxLine == null ? null : TextOverflow.ellipsis,
    );
  }
}
