import 'package:json_annotation/json_annotation.dart';
import 'package:weather_app/home/model/CurrentTempModel.dart';
import 'package:weather_app/home/model/LocationModel.dart';

part 'WeatherResponseModel.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class WeatherResponseModel {
  final LocationModel location;
  final CurrentTempModel current;

  WeatherResponseModel({required this.location, required this.current});

  factory WeatherResponseModel.fromJson(Map<String, dynamic> json) =>
      _$WeatherResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherResponseModelToJson(this);
}
