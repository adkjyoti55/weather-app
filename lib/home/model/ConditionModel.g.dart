// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ConditionModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConditionModel _$ConditionModelFromJson(Map<String, dynamic> json) {
  return ConditionModel(
    text: json['text'] as String?,
    icon: json['icon'] as String?,
    code: json['code'] as int?,
  );
}

Map<String, dynamic> _$ConditionModelToJson(ConditionModel instance) =>
    <String, dynamic>{
      'text': instance.text,
      'icon': instance.icon,
      'code': instance.code,
    };
