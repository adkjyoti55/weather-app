enum DataFetchState {
  IS_IDEAL,
  IS_LOADING,
  IS_PAGING_LOADING,
  IS_LOADED,
  ERROR_ENCOUNTERED,
  PAGING_ERROR_ENCOUNTERED,
  NO_DATA,
}

class NetworkState<T> {
  final DataFetchState state;
  final ApiResponse<T>? response;

  T? get value => response?.body;

  String get message => response?.exception?.message ?? "Something went wrong";

  NetworkState({required this.state, required this.response});

  NetworkState.loading([ApiResponse<T>? response])
      : response = response,
        state = DataFetchState.IS_LOADING;

  NetworkState.pagingLoading([ApiResponse<T>? response])
      : response = response,
        state = DataFetchState.IS_PAGING_LOADING;

  NetworkState.loaded(ApiResponse<T> response)
      : response = response,
        state = DataFetchState.IS_LOADED;

  NetworkState.error(ApiResponse<T> response)
      : response = response,
        state = DataFetchState.ERROR_ENCOUNTERED;

  NetworkState.pagingError(ApiResponse<T> response)
      : response = response,
        state = DataFetchState.PAGING_ERROR_ENCOUNTERED;

  NetworkState.ideal()
      : response = null,
        state = DataFetchState.IS_IDEAL;

  NetworkState.noData(ApiResponse<T> response)
      : response = response,
        state = DataFetchState.NO_DATA;

  NetworkState.clone(NetworkState networkState)
      : response = networkState.response as ApiResponse<T>?,
        state = networkState.state;
}

class ApiResponse<T> {
  T? body;
  ApiException? exception;

  ApiResponse({this.exception, this.body});

  ApiResponse.error({required this.exception}) : this.body = null;

  ApiResponse.response({required this.body}) : this.exception = null;
}

class ApiException implements Exception {
  String message;

  ApiException([this.message = ""]);
}
