import 'package:dio/dio.dart';
import 'package:sprintf/sprintf.dart';
import 'package:weather_app/home/model/WeatherResponseModel.dart';
import 'package:weather_app/home/repository/BaseApiRepository.dart';
import 'package:weather_app/ui/constant/constants.dart';

class WeatherApiRepository extends BaseApiRepository<WeatherResponseModel> {
  final String inputLocation;

  @override
  WeatherResponseModel parseJson(Response<dynamic> response) =>
      WeatherResponseModel.fromJson(response.data);

  @override
  String path(Map<String, dynamic>? params) {
    return sprintf(LOCATION_BY_NAME, [inputLocation]);
  }

  WeatherApiRepository({required this.inputLocation});
}
