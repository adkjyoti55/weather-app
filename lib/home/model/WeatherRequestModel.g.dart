// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'WeatherRequestModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherRequestModel _$WeatherRequestModelFromJson(Map<String, dynamic> json) {
  return WeatherRequestModel(
    location: json['location'] as String,
  );
}

Map<String, dynamic> _$WeatherRequestModelToJson(
        WeatherRequestModel instance) =>
    <String, dynamic>{
      'location': instance.location,
    };
