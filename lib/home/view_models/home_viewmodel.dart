import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:weather_app/home/model/WeatherResponseModel.dart';
import 'package:weather_app/home/repository/home_repo/WeatherApiRepository.dart';
import 'package:weather_app/network_utils/DioManager.dart';
import 'package:weather_app/network_utils/Extensions.dart';
import 'package:weather_app/network_utils/NetworkState.dart';

final homeChangeNotifierProvider =
    ChangeNotifierProvider((ref) => HomeViewModelProvider());

class HomeViewModelProvider with ChangeNotifier {
  WeatherResponseModel? get body => _joinNetworkState.response?.body;

  String get error =>
      _joinNetworkState.response?.exception?.message ?? "Something went wrong";

  NetworkState<WeatherResponseModel> _joinNetworkState = NetworkState.ideal();

  NetworkState<WeatherResponseModel> get joinNetworkState => _joinNetworkState;

  WeatherResponseModel? _weatherResponseModel;

  WeatherResponseModel? get weatherResponseModel => _weatherResponseModel;
  String _inputLocation = "";
  String get inputLocation => _inputLocation;

  String _inputLocationTemp = "";
  String get inputLocationTemp => _inputLocationTemp;

  Future<String> getInputLocation() async {
    final flutterSecureStorage = FlutterSecureStorage();
    _inputLocation =
        await flutterSecureStorage.read(key: "inputLocation") ?? "";
    return _inputLocation;
  }

  Future<String> getInputLocationTemp() async {
    final flutterSecureStorage = FlutterSecureStorage();
    final _inputLocationTemp =
        await flutterSecureStorage.read(key: "inputLocationTemp") ?? "";
    return _inputLocationTemp;
  }

  void fetchWeatherData(String inputLocation) {
    final WeatherApiRepository _weatherApiRepository =
        WeatherApiRepository(inputLocation: inputLocation);
    _weatherApiRepository.fetch(
      baseUrlType: BaseUrlType.LOCATIONBYNAME,
      apiCallback: (networkState) {
        onApiCallback<WeatherResponseModel>(
          networkState: networkState,
          onLoadedState: (loadedState) {
            onFutureNotifyListeners(() async {
              _weatherResponseModel = loadedState.response?.body;
              FlutterSecureStorage flutterSecureStorage =
                  FlutterSecureStorage();
              await flutterSecureStorage.write(
                  key: 'inputLocationTemp',
                  value:
                      weatherResponseModel?.current.temp_c?.toStringAsFixed(1));
              showMyToast(
                  _weatherResponseModel?.current.temp_c.toString() ?? "Error");
            });
          },
          onErrorState: (errorState) {
            onFutureNotifyListeners(() {
              _weatherResponseModel = errorState.response?.body;
              showMyToast("Error");
            });
          },
          onLoadingState: (loadingState) {},
        );
      },
    );
  }
}
