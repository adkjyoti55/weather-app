import 'package:dio/dio.dart';
import 'package:sprintf/sprintf.dart';
import 'package:weather_app/home/model/WeatherResponseModel.dart';
import 'package:weather_app/home/repository/BaseApiRepository.dart';
import 'package:weather_app/ui/constant/constants.dart';

class WeatherByCoordinatesRepo extends BaseApiRepository<WeatherResponseModel> {
  final double? inputLat;
  final double? inputLong;

  @override
  WeatherResponseModel parseJson(Response<dynamic> response) =>
      WeatherResponseModel.fromJson(response.data);

  @override
  String path(Map<String, dynamic>? params) {
    return sprintf(LOCATION_BY_COORDINATE, [inputLat, inputLong]);
  }

  WeatherByCoordinatesRepo({this.inputLat, this.inputLong});
}
