import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:weather_app/home/view_models/home_viewmodel.dart';
import 'package:weather_app/home/view_models/weather_view_model.dart';

class HomePage extends ConsumerWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final locationProvider = ref.watch(homeChangeNotifierProvider);
    final currentLocProvider = ref.watch(weatherChangeNotifierProvider);
    final currentLocationProvider = ref.watch(currentLocationProviderRef);
    String inputLocation;

    final locationInputController = TextEditingController();
    FlutterSecureStorage flutterSecureStorage = FlutterSecureStorage();

    // final temperatureProvider = FutureProvider<String>((ref) {
    //   final locationName = ref.watch(locationNameProvider).data?.value;
    //   if (locationName == null || locationName.isEmpty) {
    //     return fetchWeatherDataByCoordinates();
    //   } else {
    //     return _fetchWeatherByLocationName(locationName);
    //   }
    // });

    return MaterialApp(
      home: FutureBuilder<String>(
        future: locationProvider.getInputLocation(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: const CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Scaffold(
              body: Center(
                child: Text('Error: ${snapshot.error}'),
              ),
            );
          } else {
            final locationName = snapshot.data ?? "";

            return Scaffold(
              body: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextField(
                      controller: locationInputController,
                      decoration: const InputDecoration(
                        labelText: 'Location',
                      ),
                    ),
                    const SizedBox(height: 16.0),
                    ElevatedButton(
                      onPressed: () async {
                        if (locationInputController.text.isEmpty) {
                          currentLocProvider.fetchWeatherDataByCoordinates(
                              currentLocationProvider.value?.latitude,
                              currentLocationProvider.value?.longitude);
                        } else {
                          inputLocation = locationInputController.text.trim();

                          await flutterSecureStorage.write(
                              key: 'inputLocationName', value: inputLocation);

                          locationProvider.fetchWeatherData(inputLocation);
                        }
                      },
                      child: Text(locationInputController.text.isEmpty
                          ? "Save"
                          : "Update"),
                      // onPressed: () => saveLocation(context, storage),
                      // child: Text(weatherData.location != null ? 'Update' : 'Save'),
                    ),
                    const SizedBox(height: 16.0),
                    Text(
                      '${locationProvider.weatherResponseModel?.current.temp_c?.toStringAsFixed(1) ?? locationProvider.inputLocationTemp} °C ',
                      style: const TextStyle(fontSize: 24.0),
                    ),
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
