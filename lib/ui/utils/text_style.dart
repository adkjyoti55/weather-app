import 'package:flutter/material.dart';

class TextStyleUtils {
  TextStyleUtils._();

  static TextStyle titleTextStyle = const TextStyle(
    fontFamily: 'Gilroy',
    fontSize: 19.0,
    fontWeight: FontWeight.w700,
  );

  static TextStyle body1TextStyle = const TextStyle(
    fontFamily: 'Gilroy',
    fontSize: 17.0,
  );

  static TextStyle body2TextStyle = const TextStyle(
    fontFamily: 'Gilroy',
    fontSize: 15.0,
  );

  static TextStyle body3TextStyle = const TextStyle(
    fontFamily: 'Gilroy',
    fontSize: 13.0,
  );

  static TextStyle body4TextStyle = const TextStyle(
    fontFamily: 'Gilroy',
    fontSize: 12.0,
  );

  static TextStyle textStyle = const TextStyle(
    fontFamily: 'Gilroy',
  );
}
