// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CurrentTempModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CurrentTempModel _$CurrentTempModelFromJson(Map<String, dynamic> json) {
  return CurrentTempModel(
    lastUpdatedEpoch: json['lastUpdatedEpoch'] as int?,
    lastUpdated: json['lastUpdated'] as String?,
    temp_c: (json['temp_c'] as num?)?.toDouble(),
    temp_f: (json['temp_f'] as num?)?.toDouble(),
    isDay: json['isDay'] as int?,
    condition: json['condition'] == null
        ? null
        : ConditionModel.fromJson(json['condition'] as Map<String, dynamic>),
    windMph: (json['windMph'] as num?)?.toDouble(),
    windKph: (json['windKph'] as num?)?.toDouble(),
    windDegree: json['windDegree'] as int?,
    windDir: json['windDir'] as String?,
    pressureMb: json['pressureMb'] as int?,
    pressureIn: (json['pressureIn'] as num?)?.toDouble(),
    precipMm: (json['precipMm'] as num?)?.toDouble(),
    precipIn: (json['precipIn'] as num?)?.toDouble(),
    humidity: json['humidity'] as int?,
    cloud: json['cloud'] as int?,
    feelslikeC: (json['feelslikeC'] as num?)?.toDouble(),
    feelslikeF: (json['feelslikeF'] as num?)?.toDouble(),
    visKm: json['visKm'] as int?,
    visMiles: json['visMiles'] as int?,
    uv: (json['uv'] as num?)?.toDouble(),
    gustMph: (json['gustMph'] as num?)?.toDouble(),
    gustKph: (json['gustKph'] as num?)?.toDouble(),
  );
}

Map<String, dynamic> _$CurrentTempModelToJson(CurrentTempModel instance) =>
    <String, dynamic>{
      'lastUpdatedEpoch': instance.lastUpdatedEpoch,
      'lastUpdated': instance.lastUpdated,
      'temp_c': instance.temp_c,
      'temp_f': instance.temp_f,
      'isDay': instance.isDay,
      'condition': instance.condition,
      'windMph': instance.windMph,
      'windKph': instance.windKph,
      'windDegree': instance.windDegree,
      'windDir': instance.windDir,
      'pressureMb': instance.pressureMb,
      'pressureIn': instance.pressureIn,
      'precipMm': instance.precipMm,
      'precipIn': instance.precipIn,
      'humidity': instance.humidity,
      'cloud': instance.cloud,
      'feelslikeC': instance.feelslikeC,
      'feelslikeF': instance.feelslikeF,
      'visKm': instance.visKm,
      'visMiles': instance.visMiles,
      'uv': instance.uv,
      'gustMph': instance.gustMph,
      'gustKph': instance.gustKph,
    };
